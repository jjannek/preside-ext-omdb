component {

    property name="omdbMovieDataImport" inject="omdbMovieDataImport";

    /**
     * This is the task to import movie data from http://omdbapi.com
     *
     * @schedule     disabled
     * @displayName  OMDb movie data import
     * @displayGroup OMDb
     *
     */
    private boolean function importOmdbMovieData( event, rc, prc, logger ) {
        return omdbMovieDataImport.import( arguments.logger ?: NullValue() );
    }
}