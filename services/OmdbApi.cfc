/**
 * @singleton
 */
component {

// CONSTRUCTOR
    public any function init(
          string  baseUrl  = "http://www.omdbapi.com/"
        , numeric httpTimeout  = 60
    ) {

        _setBaseUrl( arguments.baseUrl );
        _setHttpTimeout( arguments.httpTimeout );

        return this;
    }

// PUBLIC METHODS
    public array function searchAll(
          required string s          // Movie title to search for.
        ,          string y     = "" // Year of release.
    ) {
        var searchResult = search( s=arguments.s, y=arguments.y, page=1 );
        var movies = searchResult.search ?: [];
        var totalRecordcount = searchResult.totalResults ?: movies.len();

        if ( totalRecordCount <= movies.len() ) {
            return movies;
        }

        var result = movies;
        var page = 1;

        while ( movies.len() < totalRecordcount ) {
            page++;
            searchResult = search( s=arguments.s, y=arguments.y, page=page );
            result.append( searchResult.search, true );
        }

        return result;
    }

    public struct function search(
          required string s          // Movie title to search for.
        ,          string y     = "" // Year of release.
        ,          numeric page = 1  // Page number to return.
    ) {
        return _getMovies( argumentCollection = arguments );
    }

    public struct function getById(
          required string  i                  // A valid IMDb ID (e.g. tt1285016)
        ,          string  y        = ""      // Year of release.
        ,          string  plot     = "short" // Return short or full plot. valid options: short, full
        ,          boolean tomatoes = false   // Include Rotten Tomatoes ratings.
    ) {
        return _getMovie( argumentCollection = arguments );
    }

    public struct function getByTitle(
          required string  t                  // Movie title to search for. (exact search)
        ,          string  y        = ""      // Year of release.
        ,          string  plot     = "short" // Return short or full plot. valid options: short, full
        ,          boolean tomatoes = false   // Include Rotten Tomatoes ratings.
    ) {
        return _getMovie( argumentCollection = arguments );
    }

// PRIVATE HELPERS
    private struct function _getMovie() {
        var result = _restCall( argumentCollection = arguments );

        var success = result.response ?: false;

        if ( _isSuccessResponse( result ) ) {
            result.delete( "response" );
            result.delete( "type" );
        }
        else {
            result.response = false;
        }

        return result;
    }

    private struct function _getMovies() {
        var result = _restCall( argumentCollection = arguments );

        var success = result.response ?: false;

        if ( _isSuccessResponse( result ) ) {
            result.delete( "response" );
            for ( var movie in result.search ) {
                movie.delete( "type" );
            }
        }
        else {
            result.response = false;
        }

        return result;
    }

    private any function _restCall() {

        var httpResult = "";

        http url       = _getBaseUrl()
             method    = "get"
             timeout   = _getHttpTimeout()
             result    = "httpResult" {
            httpparam type="url" name="type" value="movie"; // currently only movie search supported
            for ( var key in arguments ) {
                
                var paramName = lCase( key );
                var paramValue = arguments[ key ];

                // hardcoded hack to get the tomatoes boolean parameter to work
                if ( paramName == "tomatoes" ) {
                    paramValue = isBoolean( paramValue ) && paramValue ? "true" : "false";
                }
                httpparam type="url" name="#paramName#" value="#paramValue#";
            }
        }

        return _processApiResponse( argumentCollection=httpResult );
    }


    private any function _processApiResponse( string filecontent="", string status_code="" ) {

        if ( arguments.status_code neq 200 ) {
            throw(
                  type      = "omdb.unexpected"
                , message   = "An unexpected response was returned from the omdb server."
                , errorCode = Val( arguments.status_code ) ? arguments.status_code : 500
            );
        }
        
        try {
            return DeserializeJSON( arguments.fileContent );
        }
        catch ( any e ) {
            throw(
                  type    = "omdb.unexpected"
                , message = "Unexpected error processing omdb response. Response body: [#arguments.fileContent#]"
            );
        }
    }

    private boolean function _isSuccessResponse( required struct result ) {
        return arguments.result.keyExists( "response" ) && result.response == "True";
    }

// GETTERS AND SETTERS
    private string function _getBaseUrl() {
        return _baseUrl;
    }
    private void function _setBaseUrl( required string baseUrl ) {
        _baseUrl = arguments.baseUrl;
    }

    private numeric function _getHttpTimeout() {
        return _httpTimeout;
    }
    private void function _setHttpTimeout( required numeric httpTimeout ) {
        _httpTimeout = arguments.httpTimeout;
    }
}