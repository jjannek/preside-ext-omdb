/**
 * @singleton
 */
component {

// CONSTRUCTOR
    /**
     * @presideObjectService.inject presideObjectService
     * @systemConfigurationService.inject systemConfigurationService
     * @omdbApi.inject omdbApi
     */
    public any function init(
          required any presideObjectService
        , required any systemConfigurationService
        , required any omdbApi
    ) {

        _setPresideObjectService( arguments.presideObjectService );
        _setSystemConfigurationService( arguments.systemConfigurationService );
        _setOmdbApi( arguments.omdbApi );

        _setKeyRenamingMap({
            overview={
                  old=[ "year"        , "imdbID" , "poster"     ]
                , new=[ "release_year", "imdb_id", "poster_url" ]
            },
            detailed={
                  old=[ "released"    , "imdbRating" , "imdbVotes"  ]
                , new=[ "release_date", "imdb_rating", "imdb_votes" ]
            },
            rotten_tomatoes_data={
                  old=[ "dvd"             , "boxOffice" , "tomatoConsensus" , "tomatoFresh" , "tomatoImage" , "tomatoMeter" , "tomatoRating" , "tomatoReviews" , "tomatoRotten" , "tomatoUrl" , "tomatoUserMeter"  , "tomatoUserRating"  , "tomatoUserReviews"   ]
                , new=[ "dvd_release_date", "box_office", "tomato_consensus", "tomato_fresh", "tomato_image", "tomato_meter", "tomato_rating", "tomato_reviews", "tomato_rotten", "tomato_url", "tomato_user_meter", "tomato_user_rating", "tomato_user_reviews" ]
            }
        });

        return this;
    }

// PUBLIC FUNCTIONS
    public boolean function import( logger ) {
        var loggerAvailable = structKeyExists( arguments, "logger" );
        var canDebug        = loggerAvailable && arguments.logger.canDebug();
        var canError        = loggerAvailable && arguments.logger.canError();
        var canInfo         = loggerAvailable && arguments.logger.canInfo();

        if ( canInfo ) {
            arguments.logger.info( "Starting import of OMDb movies..." );
        }

        var omdbSearches = _getSearches();

        if ( isEmpty( omdbSearches ) ) {
            if ( canError ) {
                arguments.logger.error( "Import of OMDb movies failed. No search criteria defined in system settings." );
            }
            return false;
        }

        for ( var omdbSearch in omdbSearches ) {
            
            if ( canInfo ) {
                arguments.logger.info( "Now searching and importing #omdbSearch.label# movies..." );
            }

            var movies = _getOmdbMovieData(
                  search                    = omdbSearch.search
                , releaseYear               = omdbSearch.release_year 
                , includeDetails            = omdbSearch.include_details 
                , includeFullPlot           = omdbSearch.include_full_plot 
                , includeRottenTomatoesData = omdbSearch.include_rotten_tomatoes_data 
                , posterAssetFolder         = omdbSearch.poster_asset_folder
                , logger                    = arguments.logger ?: nullValue()
            );

            if ( canDebug ) {
                arguments.logger.debug( serializeJson( movies) );
            }

            _syncMovies( movies, arguments.logger ?: nullValue() );
        }

        if ( canInfo ) {
            arguments.logger.info( "Import of OMDb movies completed." );
        }

        return true;
    }

// PRIVATE HELPERS
    private array function _getOmdbMovieData(
          required string search
        , string releaseYear=""
        , boolean includeDetails=false
        , boolean includeFullPlot=false
        , boolean includeRottenTomatoesData=false
        , string posterAssetFolder=""
        , any logger
    ) {
        var loggerAvailable = structKeyExists( arguments, "logger" );
        var canDebug        = loggerAvailable && arguments.logger.canDebug();
        var canError        = loggerAvailable && arguments.logger.canError();
        var canInfo         = loggerAvailable && arguments.logger.canInfo();

        var movies = _getOmdbApi().searchAll( s=arguments.search, y=arguments.releaseYear );
        var keyRenamingMap = _getKeyRenamingMap();

        if ( !arguments.includeDetails ) {
            movies = _renameKeys( movies, keyRenamingMap.overview.old, keyRenamingMap.overview.new );
            return movies;
        }

        var result = [];
        var plot = arguments.includeFullPlot ? "full" : "short";

        for ( var movie in movies ) {
            var detailedMovieData = _getOmdbApi().getById( i=movie.imdbID, plot=plot, tomatoes=arguments.includeRottenTomatoesData );

            if ( detailedMovieData.keyExists( "response" ) && !detailedMovieData.response ) {
                if ( canWarn ) {
                    arguments.logger.warn( "no movie details found for '#movie.title#' (imdb id = #movie.imdbId# - skipping record." );
                }
                continue;
            }
            _renameStructKeys( detailedMovieData, keyRenamingMap.overview.old, keyRenamingMap.overview.new );
            _renameStructKeys( detailedMovieData, keyRenamingMap.detailed.old, keyRenamingMap.detailed.new );

            if ( arguments.includeRottenTomatoesData ) {
                _renameStructKeys( detailedMovieData, keyRenamingMap.rotten_tomatoes_data.old, keyRenamingMap.rotten_tomatoes_data.new );                
            }

            // hardcoded fix for imdb voting number
            detailedMovieData.imdb_votes = replace( detailedMovieData.imdb_votes, ",", "", "all" );

            result.append( detailedMovieData );
        }

        return result;
    }

    private void function _syncMovies( required array movies, any logger ) {

        var loggerAvailable = structKeyExists( arguments, "logger" );
        var canDebug        = loggerAvailable && arguments.logger.canDebug();
        var canError        = loggerAvailable && arguments.logger.canError();
        var canInfo         = loggerAvailable && arguments.logger.canInfo();

        if ( canInfo ) {
            arguments.logger.info( "Now syncing OMDb movie records.");
        }

        // insert/update
        for ( var movie in arguments.movies ) {
            
            _syncMovie( movie );
            
            if ( canDebug ) {
                arguments.logger.debug( "Synced '#movie.title#'.");
            }
        }

        if ( canInfo ) {
            arguments.logger.info( "Syncing of OMDb movie records completed.");
        }
    }

    private void function _syncMovie( required struct movie ) {

        var existingMovie = _getPresideObjectService().selectData( objectName="omdb_movie", filter={ imdb_id=arguments.movie.imdb_id } );
        
        if ( existingMovie.recordCount ) {
            _getPresideObjectService().updateData(
                  objectName = "omdb_movie"
                , id         = existingMovie.id
                , data       = arguments.movie
            );
        }
        else {
            _getPresideObjectService().insertData(
                  objectName = "omdb_movie"
                , data       = arguments.movie
            );
        }  
    }

    private query function _getSearches() {
        var omdbSearchIDs = _getSystemConfigurationService().getSetting( category="omdb", setting="searches" );

        if ( isEmpty( omdbSearchIDs ) ) {
            return queryNew("dummy");
        }

        return _getPresideObjectService().selectData( objectName="omdb_search", filter={ id=listToArray( omdbSearchIDs ) } );
    }

    private array function _deleteKeys( required array records, required array names ) {

        for ( var record in arguments.records ) {
            _deleteStructKeys( record, arguments.names );
        }

        return arguments.records;
    }

    private array function _renameKeys( required array records, required array oldKeys, required array newKeys ) {

        for ( var record in arguments.records ) {
            _renameStructKeys( record, arguments.oldKeys, arguments.newKeys );
        }

        return arguments.records;
    }

    private void function _renameStructKeys( required struct s, required array oldKeys, required array newKeys ) {

        for ( var i = 1; i <= arguments.oldKeys.len(); i++ ) {
            _renameStructKey( arguments.s, arguments.oldKeys[i], arguments.newKeys[i] );
        }
    }

    private void function _renameStructKey( required struct s, required string oldKey, required string newKey ) {

        if ( !arguments.s.keyExists( arguments.oldKey ) ) {
            return;
        }

        arguments.s[ arguments.newKey ] = arguments.s[ arguments.oldKey ];
        arguments.s.delete( arguments.oldKey );
    }

    private void function _deleteStructKeys( required struct s, required array keys ) {

        for ( var key in arguments.keys ) {
            arguments.s.delete( key );
        }
    }
    
// GETTERS AND SETTERS
    private any function _getPresideObjectService() {
        return _presideObjectService;
    }
    private void function _setPresideObjectService( required any presideObjectService ) {
        _presideObjectService = arguments.presideObjectService;
    }

    private any function _getSystemConfigurationService() {
        return _systemConfigurationService;
    }
    private void function _setSystemConfigurationService( required any systemConfigurationService ) {
        _systemConfigurationService = arguments.systemConfigurationService;
    }

    private any function _getOmdbApi() {
        return _omdbApi;
    }
    private void function _setOmdbApi( required any omdbApi ) {
        _omdbApi = arguments.omdbApi;
    }

    private struct function _getKeyRenamingMap() {
        return _keyRenamingMap;
    }
    private void function _setKeyRenamingMap( required struct keyRenamingMap ) {
        _keyRenamingMap = arguments.keyRenamingMap;
    }
}