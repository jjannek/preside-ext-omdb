component {

    public void function configure() {}

    public void function preInsertObjectData( event, interceptData ) {
        _generateLabel( interceptData );
    }

    public void function preUpdateObjectData( event, interceptData ) {
        _generateLabel( interceptData );
    }

    private void function _generateLabel( interceptData ) {

        var objectName = arguments.interceptData.objectname ?: "";

        if ( objectName != "omdb_search" ) {
            return;
        }

        arguments.interceptData.data.label = arguments.interceptData.data.search;

        if ( !isEmpty( arguments.interceptData.data.release_year ) ) {
            arguments.interceptData.data.label &= " (" & arguments.interceptData.data.release_year & ")";
        }
    }
}