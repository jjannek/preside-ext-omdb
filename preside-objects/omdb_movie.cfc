/**
 * @dataManagerGroup omdb
 * @labelfield title
 * @dataManagerGridFields title,release_year,rated,genre,imdb_id,imdb_rating,imdb_votes
 */
component {
    property name="title" type="string" dbtype="varchar" maxlength=300 required=true;
    property name="plot" type="string" dbtype="text";
    property name="runtime";
    property name="website";
    property name="rated";
    property name="poster_url";
    property name="poster" relationship="many-to-one" relatedto="asset" allowedTypes="images";
    property name="release_year" type="string" dbtype="varchar" maxlength=10 required=true;
    property name="release_date";
    property name="dvd_release_date";
    property name="genre";
    property name="country";
    property name="language";
    property name="production";
    property name="director";
    property name="writer" type="string" dbtype="varchar" maxlength=600;
    property name="actors";
    property name="awards";
    property name="box_office";
    property name="metascore";
    property name="imdb_id" type="string" dbtype="varchar" maxlength=20 required=true uniqueindexes="imdb_id";
    property name="imdb_rating";
    property name="imdb_votes";
    property name="tomato_consensus" type="string" dbtype="text";
    property name="tomato_fresh";
    property name="tomato_image";
    property name="tomato_meter";
    property name="tomato_rating";
    property name="tomato_reviews";
    property name="tomato_rotten";
    property name="tomato_url";
    property name="tomato_user_meter";
    property name="tomato_user_rating";
    property name="tomato_user_reviews";
}