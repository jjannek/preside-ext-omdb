/**
 * @dataManagerGroup omdb
 * @dataManagerGridFields label,include_details,include_full_plot,include_rotten_tomatoes_data,poster_asset_folder
 */
component {
	property name="search" type="string" dbtype="varchar" maxlength=300 required=true;
	property name="release_year" type="numeric" dbtype="int";
	property name="include_details" type="boolean" dbtype="boolean" default="false";
	property name="include_full_plot" type="boolean" dbtype="boolean" default="false";
    property name="include_rotten_tomatoes_data" type="boolean" dbtype="boolean" default="false";
	property name="poster_asset_folder" relationship="many-to-one" relatedTo="asset_folder";
}