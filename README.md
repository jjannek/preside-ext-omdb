# Preside Extension: OMDb API

This is an extension for [Preside](https://www.presidecms.com) that probides a wrapper service for the [OMDb API](https://www.omdbapi.com).

In detail the extension includes the following:

* Webservice Wrapper for http://omdbapi.com
* Preside Object for movies
* System setting to allow definition of one or more searches
* Task to import/sync movies based on defined searches
* TODO: automatically download movie poster images and store them in the asset manager

## Installation

Install the extension to your application via either of the methods detailed below (Git submodule / CommandBox) and then enable the extension by opening up the Preside developer console and entering:

    extension enable preside-ext-omdb
    reload all

### Git Submodule method

From the root of your application, type the following command:

    git submodule add https://bitbucket.org/jjannek/preside-ext-omdb.git application/extensions/preside-ext-omdb

### CommandBox (box.json) method

From the root of your application, type the following command:

    box install preside-ext-omdb

## Contribution

Feel free to fork and pull request. Any other feedback is also welcome - preferable on the PresideCMS slack channel.